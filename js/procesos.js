function procesar(){
	localStorage.setItem("nombre",document.getElementById("nombre").value);
	localStorage.setItem("apellido", document.getElementById("apellido").value);
	localStorage.setItem("tipoDoc", document.getElementById("tipoDoc").value);
	localStorage.setItem("numDoc", document.getElementById("numDoc").value);
	localStorage.setItem("fecNac", document.getElementById("fecNac").value);
	localStorage.setItem("esCiv", document.getElementById("esCivil").value);
	localStorage.setItem("dir", document.getElementById("dir").value);
	localStorage.setItem("tel", document.getElementById("telefono").value);
	localStorage.setItem("email", document.getElementById("exampleInputEmail1").value);
	localStorage.setItem("perPro", document.getElementById("perPro").value);
	localStorage.setItem("forPri", document.getElementById("forPri").value);
	localStorage.setItem("forSec", document.getElementById("forSec").value);
	localStorage.setItem("univer", document.getElementById("forSup").value);
	localStorage.setItem("carr", document.getElementById("carrera").value);
	localStorage.setItem("semRea", document.getElementById("semTer").value);
	localStorage.setItem("primLen", document.getElementById("priLen").value);
	localStorage.setItem("segLen", document.getElementById("segLen").value);
	localStorage.setItem("nivLen", document.getElementById("nivId").value);
}

window.onload=function() {
	document.getElementById("nombre1").innerHTML = localStorage.getItem("nombre");
	document.getElementById("apellido1").innerHTML = localStorage.getItem("apellido");
	document.getElementById("tipoDoc1").innerHTML = localStorage.getItem("tipoDoc");
	document.getElementById("numDoc1").innerHTML = localStorage.getItem("numDoc");
	document.getElementById("fecNac1").innerHTML = localStorage.getItem("fecNac");
	document.getElementById("esCiv1").innerHTML = localStorage.getItem("esCiv");
	document.getElementById("dir1").innerHTML = localStorage.getItem("dir");
	document.getElementById("tel1").innerHTML = localStorage.getItem("tel");
	document.getElementById("email1").innerHTML = localStorage.getItem("email");
	document.getElementById("perPro1").innerHTML = localStorage.getItem("perPro");
	document.getElementById("forPri1").innerHTML = localStorage.getItem("forPri");
	document.getElementById("forSec1").innerHTML = localStorage.getItem("forSec");
	document.getElementById("univer1").innerHTML = localStorage.getItem("univer");
	document.getElementById("carr1").innerHTML = localStorage.getItem("carr");
	document.getElementById("semRea1").innerHTML = localStorage.getItem("semRea");
	document.getElementById("primLen1").innerHTML = localStorage.getItem("primLen");
	document.getElementById("segLen1").innerHTML = localStorage.getItem("segLen");
	document.getElementById("nivLen1").innerHTML = localStorage.getItem("nivLen");
	var nomComp = localStorage.getItem("nombre") + " " + localStorage.getItem("apellido");
	document.getElementById("nomCom").innerHTML = nomComp;
}